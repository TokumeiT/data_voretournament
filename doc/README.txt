This file contains the Vore Tournament credits. Entries are listed in the following format:

Name of item
URL of item
Name of author
URL of author

---------------- Coding: ----------------

Main Programmer
N/A
MirceaKitsune
http://www.furaffinity.net/user/mircea/

---------------- Models: ----------------

Anthropomorphic Wolf
http://www.blendswap.com/blends/view/79735
UntiedVerbeger, MirceaKitsune
http://www.furaffinity.net/user/untiedverbeger/

---------------- Textures: ----------------

2048x2048 Tiling Beast Fur Texture
http://opengameart.org/content/2048x2048-tiling-beast-fur-texture
bart
http://opengameart.org/users/bart

369_tile_Goo
http://www.everystockphoto.com/photo.php?imageId=8460179
Patrick Hoesly
http://www.everystockphoto.com/photographer.php?photographer_id=30411

---------------- Vector: ----------------

Wolf
http://www.openclipart.org/detail/6111/wolf-by-johnny_automatic-6111
Johnny Automatic
http://www.openclipart.org/user-detail/johnny_automatic

Buddy
http://www.openclipart.org/detail/19935/buddy-by-sheikh_tuhin-19935
Sheikh Tuhin
http://www.openclipart.org/user-detail/sheikh_tuhin

---------------- Sounds: ----------------

Swallowing Gulp
http://www.pdsounds.org/sounds/swallowing_gulp
Gregory Weir
http://www.pdsounds.org/audio/by/artist/gregory_weir

Burp
http://www.pdsounds.org/sounds/burp
Ezwa
http://www.pdsounds.org/audio/by/artist/ezwa

Man Coughing
http://www.pdsounds.org/sounds/man_coughing
Stilgar
http://www.pdsounds.org/users/stilgar

Tummy Grumble
http://www.pdsounds.org/sounds/tummy_grumble
Esther
http://www.pdsounds.org/audio/by/artist/esther

Sinister Laugh
http://www.opengameart.org/content/sinister-laugh
WeaponGuy
http://www.opengameart.org/user/301

So that's coming along...
http://www.opengameart.org/content/so-thats-coming-along
bart
http://www.opengameart.org/user/1

37 hits/punches
http://opengameart.org/content/37-hitspunches
Independent.nu
N/A

---------------- Fonts: ----------------

Peace Sans
http://www.dafont.com/peace-sans.font
Sergey Ryadovoy, Ivan Gladkikh (Jovanny Lemonad)
http://www.dafont.com/profile.php?user=194118

---------------- Special Supporters: ----------------

Smokey
https://www.patreon.com/user/creators?u=2588581

Digdug
https://www.patreon.com/user/creators?u=220075

Michael Ober
https://www.patreon.com/user/creators?u=3602919
